#!/bin/bash
# Установка  бд postgress
sudo apt-get update
sudo apt-get install -y postgresql

# доступ извне

sudo -i -u postgres psql -c "ALTER SYSTEM SET listen_addresses TO '*';"

# редактирование файла pg_hba.conf

sudo sed -i -e '$a host    replication     repluser        192.168.100.10/24       md5' /etc/postgresql/12/main/pg_hba.conf
sudo sed -i -e '$a host    replication     repluser        192.168.100.20/24       md5' /etc/postgresql/12/main/pg_hba.conf
sudo sed -i '/# IPv4 local connections:/ a \host    all             wpuser          192.168.100.30/24       md5' /etc/postgresql/12/main/pg_hba.conf

# редактирование файла postgresql.conf

sudo sed -i '/#listen_addresses = '\''localhost'\''/c\listen_addresses = '\''localhost, 192.168.100.20'\''' /etc/postgresql/12/main/postgresql.conf
sudo sed -i '/#wal_level = replica/c\wal_level = replica' /etc/postgresql/12/main/postgresql.conf
sudo sed -i '/#max_wal_senders = 10/c\max_wal_senders = 2' /etc/postgresql/12/main/postgresql.conf
sudo sed -i '/#max_replication_slots = 10/c\max_replication_slots = 2' /etc/postgresql/12/main/postgresql.conf
sudo sed -i '/#hot_standby = on/c\hot_standby = on' /etc/postgresql/12/main/postgresql.conf
sudo sed -i '/#hot_standby_feedback = off/c\hot_standby_feedback = on' /etc/postgresql/12/main/postgresql.conf

#Остановка бд
sudo systemctl stop postgresql

# Настройка репликации

cd /var/lib/postgresql/12 && sudo rm -rf main && sudo PGPASSWORD="1234" pg_basebackup -P -R -X stream -c fast -h 192.168.100.10 -U repluser -D ./main && sudo chown -R postgres:postgres main/

#запуск
sudo systemctl start postgresql