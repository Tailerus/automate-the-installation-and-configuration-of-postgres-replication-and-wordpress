Automate the installation and configuration of postgres replication and wordpress

Это решение выполняют следующую задачу:
- Установка и настройка Wordpress
- Установка Postgres и насройка репликации

Стек:
- Vagrant для поднятия машин
- Postgress
- Bash скипты и Ansible для автоматизации
- Nginx, php-fpm и Wordpress
- Плагин для ипользования бд Postgres https://github.com/gmercey/PG4WP